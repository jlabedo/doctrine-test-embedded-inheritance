<?php

namespace App\Repository;

use App\Entity\EventNotification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Ulid;

/**
 * @extends ServiceEntityRepository<EventNotification>
 *
 * @method EventNotification|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventNotification|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventNotification[]    findAll()
 * @method EventNotification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventNotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventNotification::class);
    }

    public function get(Ulid $id): EventNotification
    {
        return $this->_em->createQuery(
            'SELECT e, p
            FROM App\Entity\EventNotification e
            JOIN e.policy p
            WHERE e.id = :id')
            ->setParameter('id', $id, 'ulid')
            ->getSingleResult();
    }
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(EventNotification $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(EventNotification $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
