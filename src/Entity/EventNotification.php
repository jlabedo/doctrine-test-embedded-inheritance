<?php

namespace App\Entity;

use App\Repository\EventNotificationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity(repositoryClass: EventNotificationRepository::class)]
class EventNotification
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private Ulid $id;

    #[ORM\OneToOne(fetch: 'EAGER', orphanRemoval: true, cascade: ['PERSIST', 'REMOVE'])]
    #[ORM\JoinColumn(name: "id", referencedColumnName: "event_notification_id")]
    private EscalationPolicy $policy;

    public function __construct(
        Ulid $id,
        EscalationPolicy $policy
    )
    {
        $this->id = $id;
        $this->policy = $policy;
    }

    public function getId(): Ulid
    {
        return $this->id;
    }

    public function getPolicy(): EscalationPolicy
    {
        return $this->policy;
    }
}
