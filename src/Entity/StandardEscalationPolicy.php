<?php

namespace App\Entity;

use App\Repository\StandardEscalationPolicyRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
class StandardEscalationPolicy extends EscalationPolicy
{
}
