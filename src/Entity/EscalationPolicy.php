<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity()]
#[Orm\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    'standard' => StandardEscalationPolicy::class
])]
abstract class EscalationPolicy
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private Ulid $eventNotificationId;

    #[ORM\Column(type: 'integer')]
    private int $index;

    public function __construct(
        Ulid $eventNotificationId
    )
    {
        $this->eventNotificationId = $eventNotificationId;
        $this->index = 0;
    }

    public function getEventNotificationId(): Ulid
    {
        return $this->eventNotificationId;
    }

    public function getIndex(): ?int
    {
        return $this->index;
    }
}
