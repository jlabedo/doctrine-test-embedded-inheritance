<?php

namespace App\Tests;

use App\Entity\EventNotification;
use App\Entity\StandardEscalationPolicy;
use App\Repository\EventNotificationRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

class EventNotificationRepositoryTest extends KernelTestCase
{

    public function testSomething(): void
    {
        /** @var EntityManagerInterface */
        $em = self::getContainer()->get(EntityManagerInterface::class);

        /** @var EventNotificationRepository */
        $repository = self::getContainer()->get(EventNotificationRepository::class);

        $eventNotificationId = new Ulid();
        $eventNotification = new EventNotification($eventNotificationId, new StandardEscalationPolicy($eventNotificationId));

        $repository->save($eventNotification);

        $em->clear();

        $debugStack = new DebugStack;
        $em->getConnection()->getConfiguration()->setSQLLogger($debugStack);

        $refreshedEntity = $repository->get($eventNotificationId);

        assert($refreshedEntity !== null);
        self::assertEquals($refreshedEntity->getId(), $eventNotificationId);
        $policy = $refreshedEntity->getPolicy();
        self::assertTrue($policy instanceof StandardEscalationPolicy);
        dump($debugStack);
        self::assertCount(1, $debugStack->queries, 'Only one sql query with a join is executed');
    }
}
