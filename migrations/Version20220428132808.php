<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220428132808 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE escalation_policy (
            event_notification_id UUID NOT NULL,
            index INT NOT NULL,
            type VARCHAR(255) NOT NULL,
            PRIMARY KEY(event_notification_id)
        )');
        $this->addSql('COMMENT ON COLUMN escalation_policy.event_notification_id IS \'(DC2Type:ulid)\'');
        $this->addSql('CREATE TABLE event_notification (
            id UUID NOT NULL,
            PRIMARY KEY(id)
        )');
        $this->addSql('COMMENT ON COLUMN event_notification.id IS \'(DC2Type:ulid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE escalation_policy');
        $this->addSql('DROP TABLE event_notification');
    }
}
